# Lab5 -- Integration testing

d.usenov@innopolis.university

## Specs
    Here is InnoCar Specs:
    Budet car price per minute = 26
    Luxury car price per minute = 73
    Fixed price per km = 12
    Allowed deviations in % = 5
    Inno discount in % = 5

## BVA table


| Parameter   | Equivalence Classes      |
|------------|--------------------------|
| plan       |minute, fixed_price, nonsense |
| type       |budget, luxury, nonsense |
| distance   |<=0, >0, >planned_distance\*dev, <=planned_distance\*dev, nonsense |
| time       |<=0, >0, >planned_time\*dev, <=planned_time\*dev, nonsense |
| planned_distance|<=0, >0, nonsense|
| planned_time|<=0, >0, nonsense|
| inno_discount|yes, no, nonsence |

## Desicion table

|#|  Type  |  Plan  |  Distance  |   Time  |    Planned_distance  |    Planned_time  |    Inno_discount  | Expected | Got |
|--|--------|--------|--------|--------|--------|--------|--------|--------|--------|
|1| nonsense | minute | 100 | 100 | 100 | 100 | no | Invalid| Invalid|
|2| budget | nonsense | 100 | 100 | 100 | 100 | no | Invalid| 2600|
|3| budget | minute | nonsense | 100 | 100 | 100 | no | Invalid| 2600|
|4| budget | minute | 100 | nonsense | 100 | 100 | no | Invalid| Invalid|
|5| budget | minute | 100 | 100 | nonsense | 100 | no | Invalid| null|
|6| budget | minute | 100 | 100 | 100 | nonsense | no | Invalid| 2600|
|7| budget | minute | 100 | 100 | 100 | 100 | nonsense | Invalid| Invalid|
|8| budget | minute | -100 | 100 | 100 | 100 | no | Invalid| 2600|
|9| budget | minute | 100 | -100 | 100 | 100 | no | Invalid| 2600
|10| budget | minute | 100 | 100 | -100 | 100 | no | Invalid| Invalid|
|11| budget | minute | 100 | 100 | 100 | -100 | no | Invalid| Invalid|
|12| budget | minute | 0 | 100 | 100 | 100 | no | 2600 | 2600 |
|13| budget | minute | 100 | 0 | 100 | 100 | no | Invalid | 0 |
|14| budget | minute | 100 | 100 | 0 | 100 | no | Invalid | 2600 |
|15| budget | minute | 100 | 100 | 100 | 0 | no | Invalid | 2600|
|16| budget | minute | 100 | 100 | 100 | 100 | no | 2600 | 2600 |
|17| budget | minute | 100 | 100 | 100 | 100 | yes | 2470 | 2600 |
|18| luxury | minute | 100 | 100 | 100 | 100 | no | 7300 | 9490 |
|19| luxury | minute | 100 | 100 | 100 | 100 | yes | 6935 | 9490 |
|20| budget | minute | 120 | 120 | 100 | 100 | no | 3120 | 3120 |
|21| luxury | minute | 120 | 120 | 100 | 100 | no | 8760 | 11388 |
|22| budget | minute | 120 | 120 | 100 | 100 | yes | 2964 | 3120
|23| luxury | minute | 120 | 120 | 100 | 100 | yes | 8322 | 11388 |
|24| budget | fixed_price | 100 | 100 | 100 | 100 | no | 1200 | 1250 |
|25| luxury | fixed_price | 100 | 100 | 100 | 100 | no | Invalid | 1250 |
|26| budget | fixed_price | 120 | 100 | 100 | 100 | no | 2600 | 1666 |
|27| budget | fixed_price | 100 | 120 | 100 | 100 | no | 3120 | 2000 |
|28| budget | fixed_price | 120 | 120 | 100 | 100 | no | 2600 | 2000 |
|29| budget | fixed_price | 100 | 105 | 100 | 100 | no | 1200 | 1250 |
|30| budget | fixed_price | 105 | 105 | 100 | 100 | no | 1330 | 1250 |

## Bugs

1. If it is minute plan, if anything except time, and type, is nonsense or negative value, it gives result (forget to check parameters, before calc)
2. If time is nonsense, it gives null (Since nonsense is not number, it can not calculate)
3. Did not check for planned distance and planned time to be > 0(actually it depending on what you want, for my logic, you cannot choose plan distance and time to be zero)
4. Discount do not calculate
5. Luxury type price cannot calculate properly
6. Luxury type should not be calculated for plan fix_price
7. Fix Price did not calculate properly, it gives additional 5% value of planned_distance
